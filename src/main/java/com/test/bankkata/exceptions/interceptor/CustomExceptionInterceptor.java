package com.test.bankkata.exceptions.interceptor;

import com.test.bankkata.exceptions.core.BadRequestException;
import com.test.bankkata.exceptions.core.EntityNotFoundException;
import com.test.bankkata.exceptions.core.ForbiddenException;
import com.test.bankkata.utils.payload.response.ErrorResponse;
import lombok.NonNull;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
public class CustomExceptionInterceptor extends ResponseEntityExceptionHandler {
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    ErrorResponse handleInternalServerError(@NonNull HttpServletRequest request,
                                            @NonNull Exception ex) {
        ex.printStackTrace();
        return ErrorResponse.builder()
                .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .url(request.getRequestURI())
                .message("Internal Server Error")
                .details(Collections.singletonList(ex.getLocalizedMessage()))
                .build();
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    @ResponseBody
    ErrorResponse handleBadRequestException(@NonNull HttpServletRequest request,
                                            @NonNull BadRequestException ex) {
        ex.printStackTrace();
        return ErrorResponse.builder()
                .status(HttpStatus.BAD_REQUEST.value())
                .url(request.getRequestURI())
                .message("BAD REQUEST")
                .details(Collections.singletonList(ex.getLocalizedMessage()))
                .build();
    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseBody
    ErrorResponse handleEntityNotFoundException(@NonNull HttpServletRequest request,
                                                @NonNull EntityNotFoundException ex) {
        ex.printStackTrace();
        return ErrorResponse.builder()
                .status(HttpStatus.NOT_FOUND.value())
                .url(request.getRequestURI())
                .message("DATA NOT FOUND")
                .details(Collections.singletonList(ex.getLocalizedMessage()))
                .build();
    }

    @ResponseStatus(code = HttpStatus.FORBIDDEN)
    @ExceptionHandler(ForbiddenException.class)
    @ResponseBody
    ErrorResponse handleForbiddenExceptionException(@NonNull HttpServletRequest request,
                                                    @NonNull ForbiddenException ex) {
        ex.printStackTrace();
        return ErrorResponse.builder()
                .status(HttpStatus.FORBIDDEN.value())
                .url(request.getRequestURI())
                .message("BAD CREDENTIALS")
                .details(Collections.singletonList(ex.getLocalizedMessage()))
                .build();
    }


    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        List<String> details = new ArrayList<>();
        for (ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        return ResponseEntity
                .badRequest()
                .body(ErrorResponse.builder()
                        .status(HttpStatus.BAD_REQUEST.value())
                        .message("BAD INPUTS")
                        .details(details)
                        .build());

    }
}
