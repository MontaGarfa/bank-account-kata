package com.test.bankkata.exceptions.core;

import lombok.NonNull;

public class EntityNotFoundException extends RuntimeException {
    public EntityNotFoundException(@NonNull String entity, @NonNull String attribute, @NonNull String id) {
        super(new StringBuilder().append(entity).append(" with ").append(attribute).append(" ").append(id).append(" cannot be found in the database.").toString());
    }
}