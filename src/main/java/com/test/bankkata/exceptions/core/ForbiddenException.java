package com.test.bankkata.exceptions.core;

import lombok.NonNull;

public class ForbiddenException extends RuntimeException {
    public ForbiddenException(@NonNull String msg) {
        super(msg);
    }
}
