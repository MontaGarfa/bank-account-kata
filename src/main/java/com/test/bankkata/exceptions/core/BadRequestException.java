package com.test.bankkata.exceptions.core;

import lombok.NonNull;

public class BadRequestException extends RuntimeException {
    public BadRequestException(@NonNull String id) {
        super(id);
    }
}
