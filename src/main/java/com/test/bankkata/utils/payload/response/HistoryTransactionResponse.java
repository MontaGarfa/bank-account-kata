package com.test.bankkata.utils.payload.response;

import com.test.bankkata.model.Transaction;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
public class HistoryTransactionResponse extends ApiResponse {
    private List<Transaction> transactions;

    @Builder
    public HistoryTransactionResponse(Boolean success, String message, List<Transaction> transactions) {
        super(success, message);
        this.transactions = transactions;
    }
}
