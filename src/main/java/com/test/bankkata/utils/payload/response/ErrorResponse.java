package com.test.bankkata.utils.payload.response;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class ErrorResponse {
    private String url;
    private String message;
    private int status;
    private List<String> details;
}
