package com.test.bankkata.utils.payload.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@AllArgsConstructor
public class ClientDTO {
    @NotBlank(message = "Firstname should not be empty")
    @Size(min = 3, max = 50, message
            = "Firstname must be between 3 and 50 characters")
    private String firstName;
    @NotBlank(message = "Lastname should not be empty")
    @Size(min = 3, max = 50, message
            = "About Me must be between 3 and 50 characters")
    private String lastName;
    @NotBlank(message = "Email should not be empty")
    @Email(message = "Email should be valid")
    private String email;
    @NotBlank(message = "Password should not be empty")
    @Size(min = 8, max = 30, message
            = "Password must be between 8 and 30 characters")
    private String password;
    @NotBlank(message = "Email should not be empty")
    @Size(min = 8, max = 8, message
            = "Identity Number must be equal 8 characters")
    private String identityNumber;

}
