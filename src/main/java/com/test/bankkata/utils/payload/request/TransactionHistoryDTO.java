package com.test.bankkata.utils.payload.request;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class TransactionHistoryDTO {
    @NotBlank(message = "Account Number should not be empty")
    private String accountNumber;
    @NotBlank(message = "Password should not be empty")
    @Size(min = 8, max = 30, message
            = "Password must be between 8 and 30 characters")
    private String password;
}
