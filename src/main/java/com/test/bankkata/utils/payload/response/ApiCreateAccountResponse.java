package com.test.bankkata.utils.payload.response;

import lombok.Builder;
import lombok.Data;

@Data
public class ApiCreateAccountResponse extends ApiResponse {
    private String accountNumber;

    @Builder
    public ApiCreateAccountResponse(Boolean success, String message, String accountNumber) {
        super(success, message);
        this.accountNumber = accountNumber;
    }
}
