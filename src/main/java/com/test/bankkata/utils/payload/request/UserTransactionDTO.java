package com.test.bankkata.utils.payload.request;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.*;

@Builder
@Data
public class UserTransactionDTO {
    @NotBlank(message = "Account Number should not be empty")
    private String accountNumber;
    @NotBlank(message = "Password should not be empty")
    @Size(min = 8, max = 30, message
            = "Password must be between 8 and 30 characters")
    private String password;
    @NotNull(message = "Account Number should not be empty")
    @Min(value = 10, message = "Amount must be greater than or equal to 10")
    @Max(value = 5000, message = "Amount must be less than or equal to 5000")
    private Long amount;


}
