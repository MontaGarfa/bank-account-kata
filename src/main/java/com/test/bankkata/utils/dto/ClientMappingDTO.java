package com.test.bankkata.utils.dto;

import com.test.bankkata.model.Client;
import com.test.bankkata.utils.payload.request.ClientDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("CLIENT-CONVERTER")
public class ClientMappingDTO implements ConverterDTO<Client, ClientDTO> {


    @Override
    public Client convertFromDTO(ClientDTO clientDTO) {
        return Client.builder()
                .email(clientDTO.getEmail())
                .firstName(clientDTO.getFirstName())
                .lastName(clientDTO.getLastName())
                .password(clientDTO.getPassword())
                .identityNumber(clientDTO.getIdentityNumber())
                .build();
    }
}
