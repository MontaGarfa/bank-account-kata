package com.test.bankkata.utils.dto;

public interface ConverterDTO<T, O> {
    T convertFromDTO(O o);
}
