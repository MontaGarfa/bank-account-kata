package com.test.bankkata.services.impl;

import com.test.bankkata.exceptions.core.BadRequestException;
import com.test.bankkata.model.Account;
import com.test.bankkata.model.Transaction;
import com.test.bankkata.model.enums.TransactionType;
import com.test.bankkata.repositories.ITransactionRepository;
import com.test.bankkata.services.IAccountService;
import com.test.bankkata.services.IAuthenticationService;
import com.test.bankkata.services.ITransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;


@Service
@RequiredArgsConstructor
public class TransactionService implements ITransactionService {
    private final ITransactionRepository transactionRepository;
    private final IAccountService accountService;
    private final IAuthenticationService authenticationService;

    @Override
    public void deposit(Long amount, String accountNumber, String password) {
        Account account = authenticationService.checkUserCredential(accountNumber, password);
        Long newBalance = account.getBalance() + amount;
        Transaction transaction = Transaction.builder()
                .account(account)
                .amount(amount)
                .balance(newBalance)
                .transactionType(TransactionType.DEPOSIT)
                .transactionDate(LocalDateTime.now())
                .build();
        account.setBalance(newBalance);
        transactionRepository.save(transaction);
        accountService.updateAccount(account);
    }

    @Override
    public void withdraw(Long amount, String accountNumber, String password) {
        Account account = authenticationService.checkUserCredential(accountNumber, password);
        if (account.getBalance() < amount)
            throw new BadRequestException(String.format("the account on which the number: %s does not have sufficient funds.", accountNumber));
        Long newBalance = account.getBalance() - amount;
        Transaction transaction = Transaction.builder()
                .account(account)
                .amount(amount)
                .balance(newBalance)
                .transactionType(TransactionType.WITHDRAW)
                .transactionDate(LocalDateTime.now())
                .build();
        account.setBalance(newBalance);
        transactionRepository.save(transaction);
        accountService.updateAccount(account);
    }

    @Override
    public List<Transaction> accountTransactionHistory(String accountNumber, String password) {
        Account account = authenticationService.checkUserCredential(accountNumber, password);
        return transactionRepository.findAllByAccount(account);
    }

    @Override
    public List<Transaction> allTransactions() {
        return transactionRepository.findAll();
    }

}
