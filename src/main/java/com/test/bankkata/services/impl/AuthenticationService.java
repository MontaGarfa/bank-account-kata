package com.test.bankkata.services.impl;

import com.test.bankkata.exceptions.core.EntityNotFoundException;
import com.test.bankkata.exceptions.core.ForbiddenException;
import com.test.bankkata.model.Account;
import com.test.bankkata.repositories.IAccountRepository;
import com.test.bankkata.services.IAuthenticationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationService implements IAuthenticationService {
    private final IAccountRepository accountRepository;

    @Override
    public Account checkUserCredential(String accountNumber, String password) {
        Account account = accountRepository.findById(accountNumber).orElseThrow(() -> new EntityNotFoundException("Account", "number", accountNumber));
        if (!password.equals(account.getClient().getPassword()))
            throw new ForbiddenException("VERIFY ACCOUNT CREDENTIALS");
        return account;
    }
}
