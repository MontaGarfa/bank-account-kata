package com.test.bankkata.services.impl;

import com.test.bankkata.model.Account;
import com.test.bankkata.model.Client;
import com.test.bankkata.repositories.IAccountRepository;
import com.test.bankkata.services.IAccountService;
import com.test.bankkata.services.IClientService;
import com.test.bankkata.utils.payload.request.ClientDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AccountService implements IAccountService {
    private final IAccountRepository accountRepository;
    private final IClientService clientService;

    @Override
    public String createAccount(ClientDTO clientDTO) {
        Client client = clientService.addClient(clientDTO);
        Account account = Account.builder()
                .creationDate(LocalDateTime.now())
                .balance(0L)
                .client(client)
                .build();
        account = accountRepository.saveAndFlush(account);
        return account.getAccountNumber();
    }

    @Override
    public List<Account> retrieveAllAccounts() {
        List<Account> list = accountRepository.findAll();
        return list;
    }


    @Override
    public void updateAccount(Account account) {
        accountRepository.saveAndFlush(account);
    }

}
