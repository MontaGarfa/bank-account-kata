package com.test.bankkata.services.impl;

import com.test.bankkata.exceptions.core.BadRequestException;
import com.test.bankkata.model.Client;
import com.test.bankkata.repositories.IClientRepository;
import com.test.bankkata.services.IClientService;
import com.test.bankkata.utils.dto.ConverterDTO;
import com.test.bankkata.utils.payload.request.ClientDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ClientService implements IClientService {
    private final IClientRepository clientRepository;
    @Qualifier("CLIENT-CONVERTER")
    private final ConverterDTO<Client, ClientDTO> clientConverterDTO;

    @Override
    public boolean ifClientExistByIdentityNumber(String identityNumber) {
        return clientRepository.existsByIdentityNumber(identityNumber);
    }

    @Override
    public Client addClient(ClientDTO clientDTO) {
        if (ifClientExistByIdentityNumber(clientDTO.getIdentityNumber()))
            throw new BadRequestException(String.format("Client with identity number %s exist", clientDTO.getIdentityNumber()));
        Client client = clientConverterDTO.convertFromDTO(clientDTO);
        return clientRepository.saveAndFlush(client);
    }

    @Override
    public List<Client> retrieveAllClients() {
        List<Client> list = clientRepository.findAll();
        return list;
    }
}
