package com.test.bankkata.services;

import com.test.bankkata.model.Account;

public interface IAuthenticationService {
    Account checkUserCredential(String accountNumber, String password);
}
