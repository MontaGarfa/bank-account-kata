package com.test.bankkata.services;

import com.test.bankkata.model.Transaction;

import java.util.List;

public interface ITransactionService {
    void deposit(Long amount, String accountNumber, String password);

    void withdraw(Long amount, String accountNumber, String password);

    List<Transaction> accountTransactionHistory(String accountNumber, String password);

    List<Transaction> allTransactions();
}
