package com.test.bankkata.services;

import com.test.bankkata.model.Client;
import com.test.bankkata.utils.payload.request.ClientDTO;

import java.util.List;

public interface IClientService {
    boolean ifClientExistByIdentityNumber(String identityNumber);

    Client addClient(ClientDTO clientDTO);

    List<Client> retrieveAllClients();
}
