package com.test.bankkata.services;

import com.test.bankkata.model.Account;
import com.test.bankkata.utils.payload.request.ClientDTO;

import java.util.List;

public interface IAccountService {
    String createAccount(ClientDTO createAccountRequest);

    List<Account> retrieveAllAccounts();

    void updateAccount(Account account);

}
