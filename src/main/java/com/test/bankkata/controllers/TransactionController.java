package com.test.bankkata.controllers;

import com.test.bankkata.model.Transaction;
import com.test.bankkata.services.ITransactionService;
import com.test.bankkata.utils.payload.request.TransactionHistoryDTO;
import com.test.bankkata.utils.payload.request.UserTransactionDTO;
import com.test.bankkata.utils.payload.response.ApiResponse;
import com.test.bankkata.utils.payload.response.HistoryTransactionResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/transaction")

@RequiredArgsConstructor
public class TransactionController {
    private final ITransactionService transactionService;

    @PostMapping(value = "deposit")
    public ResponseEntity<ApiResponse> depositMoney(@RequestBody @Valid UserTransactionDTO transactionDTO) {
        transactionService.deposit(transactionDTO.getAmount(), transactionDTO.getAccountNumber(), transactionDTO.getPassword());

        return ResponseEntity.ok().body(new ApiResponse(true, "SUCCESS OF THE DEPOSIT"));
    }

    @PostMapping(value = "withdraw")
    public ResponseEntity<ApiResponse> withdrawMoney(@RequestBody @Valid UserTransactionDTO transactionDTO) {
        transactionService.withdraw(transactionDTO.getAmount(), transactionDTO.getAccountNumber(), transactionDTO.getPassword());
        return ResponseEntity.ok().body(new ApiResponse(true, "SUCCESS OF THE WITHDRAW"));
    }

    @GetMapping(value = "all")
    public ResponseEntity<List<Transaction>> getAll() {
        return ResponseEntity.ok().body(transactionService.allTransactions());
    }

    @PostMapping(value = "history")
    public ResponseEntity<HistoryTransactionResponse> fetchAccountHistory(@RequestBody TransactionHistoryDTO transactionHistoryDTO) {
        List<Transaction> transactions = transactionService.accountTransactionHistory(transactionHistoryDTO.getAccountNumber(), transactionHistoryDTO.getPassword());

        return ResponseEntity.ok().body(HistoryTransactionResponse.builder()
                .transactions(transactions)
                .message("FETCH HISTORY WITH SUCCESS")
                .success(true)
                .build());
    }
}
