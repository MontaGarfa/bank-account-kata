package com.test.bankkata.controllers;

import com.test.bankkata.model.Account;
import com.test.bankkata.services.IAccountService;
import com.test.bankkata.utils.payload.request.ClientDTO;
import com.test.bankkata.utils.payload.response.ApiCreateAccountResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/account")
@RequiredArgsConstructor
public class AccountController {
    private final IAccountService accountService;

    @PostMapping(value = "create")
    public ResponseEntity<ApiCreateAccountResponse> createAccount(@RequestBody @Valid ClientDTO createAccountRequest) {
        String accountNumber = accountService.createAccount(createAccountRequest);
        return ResponseEntity.ok().body(ApiCreateAccountResponse
                .builder()
                .accountNumber(accountNumber)
                .success(true)
                .message("CREATE ACCOUNT WITH SUCCESS")
                .build());
    }

    @GetMapping(value = "all")
    public ResponseEntity<List<Account>> retrieveAllAccounts() {
        return ResponseEntity.ok().body(accountService.retrieveAllAccounts());
    }
}
