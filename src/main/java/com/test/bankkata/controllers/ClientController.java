package com.test.bankkata.controllers;

import com.test.bankkata.model.Client;
import com.test.bankkata.services.IClientService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1/client")
@RequiredArgsConstructor
public class ClientController {
    private final IClientService clientService;

    @GetMapping(value = "all")

    public ResponseEntity<List<Client>> retrieveAllClients() {
        ResponseEntity entity = ResponseEntity.ok().body(clientService.retrieveAllClients());
        return entity;
    }
}
