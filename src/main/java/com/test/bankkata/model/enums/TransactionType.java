package com.test.bankkata.model.enums;

public enum TransactionType {
    WITHDRAW,
    DEPOSIT
}
