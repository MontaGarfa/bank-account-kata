package com.test.bankkata.model;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.test.bankkata.model.enums.TransactionType;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "transaction")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Transaction {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(name = "Transaction_UUID", value = "org.hibernate.id.uuid.CustomVersionOneStrategy")
            })
    private String transactionId;
    private Long amount;
    private Long balance;
    private TransactionType transactionType;
    private LocalDateTime transactionDate;
    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    @JoinColumn(name = "accountNumber", nullable = false)
    private Account account;
}
