package com.test.bankkata.repositories;

import com.test.bankkata.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IAccountRepository extends JpaRepository<Account, String> {
    boolean existsByAccountNumber(String accountNumber);
}
