package com.test.bankkata.repositories;

import com.test.bankkata.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClientRepository extends JpaRepository<Client, String> {
    boolean existsByIdentityNumber(String identityNumber);
}
