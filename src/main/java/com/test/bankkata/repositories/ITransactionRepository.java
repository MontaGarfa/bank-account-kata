package com.test.bankkata.repositories;

import com.test.bankkata.model.Account;
import com.test.bankkata.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ITransactionRepository extends JpaRepository<Transaction, String> {
    List<Transaction> findAllByAccount(Account account);
}
