package com.test.bankkata.services;

import com.test.bankkata.exceptions.core.BadRequestException;
import com.test.bankkata.model.Client;
import com.test.bankkata.repositories.IClientRepository;
import com.test.bankkata.services.impl.ClientService;
import com.test.bankkata.services.impl.TransactionService;
import com.test.bankkata.utils.dto.ConverterDTO;
import com.test.bankkata.utils.payload.request.ClientDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ClientServiceTest {
    private AutoCloseable closeable;

    private IClientService clientService;

    @Mock
    private IClientRepository clientRepository;
    @Mock
    private ConverterDTO<Client, ClientDTO> clientConverterDTO;

    private final String IDENTITY_NUMBER = "ID_12345678";
    private final String CLIENT_NUMBER = "CLIENT_12345678";

    @BeforeEach
    void setUp(){
        closeable = MockitoAnnotations.openMocks(this);

        this.clientService=new ClientService(clientRepository,clientConverterDTO);
    }
    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }
    @Test
    public void testAddClientOK(){
        ClientDTO clientDTO = ClientDTO.builder()
                .email("test@test.com")
                .firstName("firstName")
                .lastName("lastName")
                .identityNumber(IDENTITY_NUMBER)
                .password("123546789")
                .build();

        when(this.clientService.ifClientExistByIdentityNumber(IDENTITY_NUMBER)).thenReturn(false);

        Client client1 = Client.builder()
                .clientId(CLIENT_NUMBER)
                .email(clientDTO.getEmail())
                .password(clientDTO.getPassword())
                .lastName(clientDTO.getLastName())
                .firstName(clientDTO.getFirstName())
                .identityNumber(clientDTO.getIdentityNumber())
                .build();
        when(this.clientConverterDTO.convertFromDTO(clientDTO)).thenReturn(client1);
        when(this.clientRepository.saveAndFlush(client1)).thenReturn(client1);

        Client client = this.clientService.addClient(clientDTO);
        assertEquals(CLIENT_NUMBER,client.getClientId());
        assertEquals(IDENTITY_NUMBER,client.getIdentityNumber());

    };

    @Test
    public void testAddClientExistAccountNumber(){
        ClientDTO clientDTO = ClientDTO.builder()
                .email("test@test.com")
                .firstName("firstName")
                .lastName("lastName")
                .identityNumber(IDENTITY_NUMBER)
                .password("123546789")
                .build();
        when(this.clientService.ifClientExistByIdentityNumber(IDENTITY_NUMBER)).thenReturn(true);
        Exception exception = assertThrows(BadRequestException.class, () -> {
            this.clientService.addClient(clientDTO);
        });
        assertEquals(String.format("Client with identity number %s exist", IDENTITY_NUMBER),exception.getLocalizedMessage());
    };
    @Test
    public void testRetrieveAllClients(){
        Client client = Client.builder().clientId(CLIENT_NUMBER).build();
        when(this.clientRepository.findAll()).thenReturn(Collections.singletonList(client));
        List<Client> clientList = this.clientService.retrieveAllClients();
        assertEquals(1,clientList.size());
        assertEquals(CLIENT_NUMBER,clientList.get(0).getClientId());
    }

}
