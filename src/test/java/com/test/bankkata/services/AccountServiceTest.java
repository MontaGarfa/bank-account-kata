package com.test.bankkata.services;

import com.test.bankkata.model.Account;
import com.test.bankkata.model.Client;
import com.test.bankkata.repositories.IAccountRepository;
import com.test.bankkata.services.impl.AccountService;
import com.test.bankkata.utils.payload.request.ClientDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    private AutoCloseable closeable;
    private final String IDENTITY_NUMBER = "ID_12345678";
    private final String CLIENT_NUMBER = "CLIENT_12345678";
    private final String ACCOUNT_NUMBER = "ACCOUNT_12345678";

    private IAccountService accountService;
    @Mock
    private IAccountRepository accountRepository;
    @Mock
    private IClientService clientService;
    @BeforeEach
    void setUp(){
        closeable = MockitoAnnotations.openMocks(this);
        this.accountService = new AccountService(accountRepository,clientService);
    }
    @AfterEach
    public void closeService() throws Exception {
        closeable.close();
    }
    @Test
    public void testCreateAccountOK(){
        ClientDTO clientDTO = ClientDTO.builder().identityNumber(IDENTITY_NUMBER).build();
        Client client = Client.builder().clientId(CLIENT_NUMBER).identityNumber(IDENTITY_NUMBER).build();
        Account account = Account.builder()
                .accountNumber(ACCOUNT_NUMBER)
                .creationDate(LocalDateTime.now())
                .balance(0L)
                .client(client)
                .build();
        when(this.clientService.addClient(clientDTO)).thenReturn(client);
        when(this.accountRepository.saveAndFlush(any(Account.class))).thenReturn(account);
        String accountNumber = this.accountService.createAccount(clientDTO);
        assertEquals(ACCOUNT_NUMBER,accountNumber);
    }
    @Test
    public void testRetrieveAllAccounts(){
        Account account = Account.builder().accountNumber(ACCOUNT_NUMBER).build();
        when(this.accountRepository.findAll()).thenReturn(Collections.singletonList(account));
        List<Account> accounts = this.accountService.retrieveAllAccounts();
        assertEquals(1,accounts.size());
        assertEquals(ACCOUNT_NUMBER,accounts.get(0).getAccountNumber());
    }

}
