package com.test.bankkata.services;

import com.test.bankkata.exceptions.core.BadRequestException;
import com.test.bankkata.exceptions.core.EntityNotFoundException;
import com.test.bankkata.exceptions.core.ForbiddenException;
import com.test.bankkata.model.Account;
import com.test.bankkata.model.Client;
import com.test.bankkata.repositories.IAccountRepository;
import com.test.bankkata.services.impl.AuthenticationService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceTest {
    private AutoCloseable closeable;
    private final String ACCOUNT_NUMBER = "CLIENT_12345678";
    private final String ACCOUNT_PASSWORD = "12345678";


    private IAuthenticationService authenticationService;
    @Mock
    private IAccountRepository accountRepository;
    @BeforeEach
    void setUp(){
        closeable = MockitoAnnotations.openMocks(this);

        this.authenticationService = new AuthenticationService(accountRepository);
    }
    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }
    @Test
    void testCheckUserCredentialOk(){
        when(this.accountRepository.findById(ACCOUNT_NUMBER)).thenReturn(Optional.of(Account.builder().accountNumber(ACCOUNT_NUMBER).client(Client.builder().password(ACCOUNT_PASSWORD).build()).build()));
        Account account= this.authenticationService.checkUserCredential(ACCOUNT_NUMBER,ACCOUNT_PASSWORD);
        assertEquals(ACCOUNT_NUMBER,account.getAccountNumber());
        assertEquals(ACCOUNT_PASSWORD,account.getClient().getPassword());

    };
    @Test
    void testCheckUserCredentialNotExist(){
        when(this.accountRepository.findById(ACCOUNT_NUMBER)).thenReturn(Optional.empty());
        Exception exception = assertThrows(EntityNotFoundException.class, () -> {
            this.authenticationService.checkUserCredential(ACCOUNT_NUMBER,ACCOUNT_PASSWORD);
        });
        assertEquals(String.format("Account with number %s cannot be found in the database.", ACCOUNT_NUMBER),exception.getLocalizedMessage());

    };
    @Test
    void testCheckUserCredentialInvalidPassword(){
        when(this.accountRepository.findById(ACCOUNT_NUMBER)).thenReturn(Optional.of(Account.builder().accountNumber(ACCOUNT_NUMBER).client(Client.builder().password("12345").build()).build()));
        Exception exception = assertThrows(ForbiddenException.class, () -> {
            this.authenticationService.checkUserCredential(ACCOUNT_NUMBER,ACCOUNT_PASSWORD);
        });
        assertEquals("VERIFY ACCOUNT CREDENTIALS",exception.getLocalizedMessage());

    };
}
