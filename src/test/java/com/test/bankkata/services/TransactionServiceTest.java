package com.test.bankkata.services;

import com.test.bankkata.exceptions.core.BadRequestException;
import com.test.bankkata.model.Account;
import com.test.bankkata.model.Transaction;
import com.test.bankkata.repositories.ITransactionRepository;
import com.test.bankkata.services.impl.TransactionService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {
    private final String ACCOUNT_NUMBER = "ACCOUNT_12345678";
    private final String ACCOUNT_PASSWORD = "12345678";

    private final Long AMOUNT = 100L;
    private AutoCloseable closeable;

    private ITransactionService transactionService;
    @Mock
    private ITransactionRepository transactionRepository;
    @Mock
    private IAccountService accountService;
    @Mock
    private IAuthenticationService authenticationService;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
        this.transactionService = new TransactionService(transactionRepository, accountService, authenticationService);
    }

    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }
    @Test
    void testDepositOK(){
        when(this.authenticationService.checkUserCredential(ACCOUNT_NUMBER,ACCOUNT_PASSWORD)).thenReturn(Account.builder().balance(AMOUNT).build());
        this.transactionService.deposit(AMOUNT,ACCOUNT_NUMBER,ACCOUNT_PASSWORD);
    }
    @Test
    void testWithdrawOK(){
        when(this.authenticationService.checkUserCredential(ACCOUNT_NUMBER,ACCOUNT_PASSWORD)).thenReturn(Account.builder().balance(AMOUNT).build());
        this.transactionService.deposit(AMOUNT,ACCOUNT_NUMBER,ACCOUNT_PASSWORD);
    }
    @Test
    void testWithdrawKO(){
        when(this.authenticationService.checkUserCredential(ACCOUNT_NUMBER,ACCOUNT_PASSWORD)).thenReturn(Account.builder().balance(AMOUNT).build());
        Exception exception = assertThrows(BadRequestException.class, () -> {
            this.transactionService.withdraw(AMOUNT*2,ACCOUNT_NUMBER,ACCOUNT_PASSWORD);
        });
        assertEquals(String.format("the account on which the number: %s does not have sufficient funds.", ACCOUNT_NUMBER),exception.getLocalizedMessage());
    }


}
