package com.test.bankkata.utils;

import com.test.bankkata.model.Client;
import com.test.bankkata.services.impl.ClientService;
import com.test.bankkata.utils.dto.ClientMappingDTO;
import com.test.bankkata.utils.dto.ConverterDTO;
import com.test.bankkata.utils.payload.request.ClientDTO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class ClientMappingDtoTest {
    private AutoCloseable closeable;
    private final String IDENTITY_NUMBER = "ID_12345678";
    private final String FIRSTNAME = "FIRSTNAME";
    private final String LASTNAME = "LASTNAME";


    private ConverterDTO<Client, ClientDTO> clientClientDTOConverterDTO;
    @BeforeEach
    void setUp(){
        closeable = MockitoAnnotations.openMocks(this);

        this.clientClientDTOConverterDTO=new ClientMappingDTO();
    }
    @AfterEach
    void closeService() throws Exception {
        closeable.close();
    }
    @Test
    public void testConvertFromDto(){
        Client client = this.clientClientDTOConverterDTO.convertFromDTO(ClientDTO.builder().identityNumber(IDENTITY_NUMBER).firstName(FIRSTNAME).lastName(LASTNAME).build());
        assertEquals(IDENTITY_NUMBER,client.getIdentityNumber());
        assertEquals(FIRSTNAME,client.getFirstName());
        assertEquals(LASTNAME,client.getLastName());

    }
    

}
