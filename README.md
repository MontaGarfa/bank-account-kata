<div id="top"></div>



<!-- PROJECT LOGO -->
<br />
<div align="center">

<h3 align="center">Bank Account KATA </h3>


</div>






<!-- ABOUT THE PROJECT -->
## About The Project

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [Java 11]()
* [Spring Boot 2]()
* [Mockito 4]()
* [Junit 5]()
* [Maven]()


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* Install
  ```sh
  Java 11, Maven 3.8
  ```

### Installation

1. Build The project
   ```sh
   mvn clean install
   ```

2. Run The application
   ```sh
   cd target && java -jar bank-kata-0.0.1-SNAPSHOT.jar;
   ```


4. Test the application with input => import postman collection


<p align="right">(<a href="#top">back to top</a>)</p>


